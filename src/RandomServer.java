
package if4030.corba;

import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

class RandomServer extends RandomPOA {
  private int min = 1;
  private int max = 100;

  public int minValue() {
    return min;
  }

  public int maxValue() {
    return max;
  }

  public void setBounds( int min, int max ) {
    if( min <=max ) {
      this.min = min;
      this.max = max;
    }
  }
  public int nextRandom() {
    double alea = Math.random();
    return min + ( int ) (( max - min + 1 ) * alea );
  }

  public static void main( String args[] ) {
    try {
      Properties props = new Properties();
      props.put( "org.omg.CORBA.ORBInitialPort", "1050" );
      props.put( "org.omg.CORBA.ORBInitialHost", "localhost" );
      ORB orb = ORB.init( args, props );
      POA rootpoa = POAHelper.narrow( orb.resolve_initial_references( "RootPOA" ));
      rootpoa.the_POAManager().activate();

      RandomServer server = new RandomServer();
      org.omg.CORBA.Object ref = rootpoa.servant_to_reference( server );

      NamingContextExt naming = NamingContextExtHelper.narrow( orb.resolve_initial_references( "NameService" ));
      NameComponent path[] = naming.to_name( "RandomServer" );
      naming.rebind( path, ref );

      orb.run();
    }
    catch( Exception e ) {
      System.err.println( "ERROR: " + e );
      e.printStackTrace(System.out);
    }
  }
}
