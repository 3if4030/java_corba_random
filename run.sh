#!/bin/sh

export PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin:/usr/lib/jvm/java-8-openjdk-amd64/jre/bin:$PATH

idlj -fall Random.idl
cp src/*.java if4030/corba/
javac if4030/corba/*.java
echo "Starting orb"
orbd -ORBInitialPort 1050 -ORBInitialHost localhost &
sleep 2
echo "Starting server"
java if4030.corba.RandomServer &
sleep 2

java if4030.corba.RandomClient
java if4030.corba.RandomClient -2 2
java if4030.corba.RandomClient
