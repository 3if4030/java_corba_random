
package if4030.corba;

import java.util.Properties;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;

class RandomClient {

  public static void main( String args[] ) {
    try {
      Properties props = new Properties();
      props.put( "org.omg.CORBA.ORBInitialPort", "1050" );
      props.put( "org.omg.CORBA.ORBInitialHost", "localhost" );
      ORB orb = ORB.init( args, props );
      NamingContextExt naming = NamingContextExtHelper.narrow( orb.resolve_initial_references( "NameService" ));
      Random random = RandomHelper.narrow( naming.resolve_str( "RandomServer" ));
      if( args.length >= 2 ) {
        random.setBounds( Integer.valueOf( args[0] ), Integer.valueOf( args[1] ));
      }
      else {
        for( int i = 0; i < 10; ++i ) {
          System.out.print( random.nextRandom() + " " );
        }
        System.out.println();
      }
    }
    catch( Exception e ) {
      System.err.println( "ERROR: " + e );
      e.printStackTrace(System.out);
    }
  }

}
